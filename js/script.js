/* Captura tamanho e largura da tela do usuário */
var x = $(window).height();
var y = $(window).width();

/*Ativa efeito de expandir as imagens*/
$(document).ready(function(){
    $('.materialboxed').materialbox();
});

/* Ajusta tamanho de todas as sections para o tamanho da tela do usuário*/
$(document).find('section').each(function( index ) {
        $(this).css("height", x - 45);
});

$('#full-carousel').css("max-height", x - 45);

/* Captura meio da primeira section para alinhar o texto*/
midSection = x/2;

/* Configura os dois carrosseis full e portifolio*/
$(document).ready(function(){
    $('#full-carousel').carousel({fullWidth: true, indicators:true});
    $('#carousel-portfolio').carousel({shift:100, indicators:false});

    setInterval(function(){
        $('#carousel-portfolio').carousel('next');
    }, 1700);

    setInterval(function(){
        $('#full-carousel').carousel('next');
    }, 5000);
});

/* Configura todos os links do menu para rolagem*/
$("#menu-portfolio").click(function() {
    $('html, body').animate({
        scrollTop: $("#section-portfolio").offset().top - $('nav').height()
    }, 2000);
})

$("#menu-home").click(function() {
    $('html, body').animate({
        scrollTop: $("#section-home").offset().top - $('nav').height()
    }, 2000);
})

$("#menu-clientes").click(function() {
    $('html, body').animate({
        scrollTop: $("#section-clientes").offset().top - $('nav').height()
    }, 2000);
})

$("#menu-contato").click(function() {
    $('html, body').animate({
        scrollTop: $("#section-contato").offset().top - $('nav').height()
    }, 2000);
})

$("#menu-servicos").click(function() {
    $('html, body').animate({
        scrollTop: $("#section-servico").offset().top - $('nav').height()
    }, 2000);
})